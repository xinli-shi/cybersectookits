# -*- coding: utf-8 -*-
# @Time : 2023/5/1 16:27
# @Author : Su Hao
# @File : AE.py
# @Software : PyCharm
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras import regularizers
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np

# # 读入聚合流量特征
# features = pd.read_csv('output_features.csv')
# labels = features['label']
# # 0：未加密正常流量 1：加密正常流量 2：攻击流量
# mask_0 = (features['label'] == 0)
# mask_1 = (features['label'] == 1)
# mask_2 = (features['label'] == 2)
#
# features_0 = features[mask_0].drop('label', axis=1)
# features_1 = features[mask_1].drop('label', axis=1)
# features_2 = features[mask_2].drop('label', axis=1)
#
# labels_0 = labels[mask_0]
# labels_1 = labels[mask_1]
# labels_2 = labels[mask_2]

node_features = pd.read_csv('output_features.csv')
labels = node_features['label']
mask_0 = (node_features['label'] == 0)
mask_1 = (node_features['label'] == 1)
features_0 = node_features[mask_0].drop('label', axis=1)
features_1 = node_features[mask_1].drop('label', axis=1)


# 定义自编码器模型
class AE:
    def __init__(self, _input_dim, _encoding_dim=64, _sparsity_factor=0.1, _l1_reg=1e-5):
        # 保存参数为实例变量
        self.input_dim = _input_dim
        self.encoding_dim = _encoding_dim
        self.sparsity_factor = _sparsity_factor
        self.l1_reg = _l1_reg

        # 定义输入层
        input_layer = Input(shape=(_input_dim,))

        # 定义编码层
        encoding_layer = Dense(_encoding_dim, activation='relu',
                               activity_regularizer=regularizers.l1(_l1_reg),
                               name='encoder')(input_layer)

        # 定义解码层
        decoding_layer = Dense(_input_dim, activation='sigmoid')(encoding_layer)

        # 定义整个模型
        self.model = Model(input_layer, decoding_layer)

        # 定义损失函数和优化器
        self.model.compile(loss='mse', optimizer='adam')

    def train(self, x_train, x_val, epochs=100, batch_size=32):
        self.model.fit(x_train, x_train, epochs=epochs, batch_size=batch_size, validation_data=(x_val, x_val))

    def encode(self, x):
        # 获取编码器的输出
        encoder = Model(inputs=self.model.input, outputs=self.model.get_layer('encoder').output)
        return encoder.predict(x)

    def decode(self, x):
        return self.model.predict(x)


# 假设数据集保存在data变量中
x_train, x_test = train_test_split(features_0, test_size=0.3, random_state=42)
x_x_train, x_x_val = train_test_split(x_train, test_size=0.3, random_state=24)
unencrypted_model = AE(_input_dim=features_0.shape[1])
unencrypted_model.train(x_train=x_x_train, x_val=x_x_val)

# 对正常数据进行重构并计算重构误差
recon_normal_data = unencrypted_model.decode(x_train)
normal_errors = np.mean(np.square(x_train - recon_normal_data), axis=1)

# 计算最大重构误差作为检测阈值
threshold = np.max(normal_errors)
print(threshold)

# 对测试数据进行重构并计算重构误差
recon_test_data = unencrypted_model.decode(x_test)
test_errors = np.mean(np.square(x_test - recon_test_data), axis=1)

# 判断是否为异常流量
is_anomaly = test_errors > threshold
num_anomalies = np.sum(is_anomaly)
print("Number of anomalies detected:", num_anomalies)

# 对其他测试数据进行重构并计算重构误差
print("Number of all samples:", features_1.shape[0])
recon_test_data = unencrypted_model.decode(features_1)
test_errors = np.mean(np.square(features_1 - recon_test_data), axis=1)

# 判断是否为异常流量
is_anomaly = test_errors >= threshold
num_anomalies = np.sum(is_anomaly)

print("Number of anomalies detected:", features_1.shape[0])