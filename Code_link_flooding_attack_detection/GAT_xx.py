# -*- coding: utf-8 -*-
# @Time : 2023/4/25 10:27
# @Author : Su Hao
# @File : GAT_xx.py
# @Software : PyCharm
import numpy as np
import dgl
import pandas as pd
import torch
import scipy.sparse as sp
from dgl.nn.pytorch import GATConv
import torch.optim as optim
from GCN_RF import encode_onehot
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score


def load_data():
    idx_features_labels = pd.read_csv('./data/nodes_features.csv')
    # 提取出特征和标签
    features = sp.csr_matrix(idx_features_labels[['fwd_pkts_payload.max', 'flow_pkts_payload.avg', 'down_up_ratio', 'fwd_pkts_tot', 'fwd_pkts_payload.tot', 'flow_pkts_per_sec']], dtype=np.float32)
    labels = encode_onehot(idx_features_labels['Label'])
    features = torch.FloatTensor(np.array(features.todense()))  # tensor为pytorch常用的数据结构
    labels = torch.LongTensor(np.where(labels)[1])

    return features, labels


# 定义GAT模型
class GAT(torch.nn.Module):
    def __init__(self, in_feats, hidden_feats, num_classes):
        super(GAT, self).__init__()
        self.conv1 = GATConv(in_feats, hidden_feats, num_heads=8)
        self.conv2 = GATConv(hidden_feats * 8, num_classes, num_heads=1)

    def forward(self, _graph, inputs):
        h = self.conv1(_graph, inputs).flatten(1)
        h = torch.nn.functional.relu(h)
        h = self.conv2(_graph, h).mean(1)
        return torch.nn.functional.log_softmax(h, dim=1)


# 从CSV文件中读取边列表
edge_list = pd.read_csv('./data/edges.csv')
src_list = edge_list['idx'].tolist()
dst_list = edge_list['relative_idx'].tolist()

# 创建无向图，并添加自环
graph = dgl.graph((src_list, dst_list), num_nodes=len(pd.read_csv('./data/nodes_features.csv')))
graph = dgl.add_self_loop(graph)

# 加载节点特征和标签
node_features, labels = load_data()

# 定义模型、优化器和损失函数
model = GAT(in_feats=node_features.shape[1], hidden_feats=16, num_classes=2)
optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-4)


def evaluate(output, labels):
    labels = np.array(labels.cpu())
    output = np.array(output.max(1)[1].cpu())
    acc = accuracy_score(labels, output)
    pre = precision_score(labels, output, zero_division=1)
    r = recall_score(labels, output, zero_division=1)
    f1 = f1_score(labels, output, zero_division=1)
    return acc, pre, r, f1


# 定义训练函数
def train(epoch):
    model.train()
    optimizer.zero_grad()  # 清零梯度
    outputs = model(graph, node_features)[idx_train]
    loss_train = torch.nn.functional.nll_loss(outputs, labels[idx_train])
    loss_train.backward()
    optimizer.step()

    model.eval()
    outputs = model(graph, node_features)

    loss_val = torch.nn.functional.nll_loss(outputs[idx_val], labels[idx_val])  # 验证集的损失函数
    acc_val, precision_val, recall_val, f1_val = evaluate(outputs[idx_val], labels[idx_val])
    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_val: {:.4f}'.format(loss_val.item()),
          'acc_val: {:.4f}'.format(acc_val.item()),
          'precision_val: {:.4f}'.format(precision_val.item()),
          'recall_val: {:.4f}'.format(recall_val.item()),
          'f1_val: {:.4f}'.format(f1_val.item()))


# 定义测试函数
def test():
    model.eval()
    outputs = model(graph, node_features)[idx_test]
    loss_test = torch.nn.functional.nll_loss(outputs, labels[idx_test])
    acc_test, precision_test, recall_test, f1_test = evaluate(outputs[idx_test], labels[idx_test])
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()),
          "precision= {:.4f}".format(precision_test.item()),
          "recall= {:.4f}".format(recall_test.item()),
          "f1= {:.4f}".format(f1_test.item()))
    return acc_test, precision_test, recall_test, f1_test


# 5折交叉验证
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=322)
acc_tests, precision_tests, recall_tests, f1_tests = [], [], [], []
t_total = []
for train1, i in kfold.split(range(node_features.shape[0]), labels):
    idx_test = list(set(range(node_features.shape[0])).difference(set(train1)))
    idx_train, idx_val, _, _ = train_test_split(train1, labels[train1], random_state=11, test_size=0.1)
    idx_train = torch.LongTensor(idx_train)
    idx_val = torch.LongTensor(idx_val)
    idx_test = torch.LongTensor(idx_test)
#     for epoch in range(15):
#         train(epoch)
#     print("Optimization Finished!")
#     acc_test, precision_test, recall_test, f1_test = test()
#     acc_tests.append(acc_test)
#     precision_tests.append(precision_test)
#     recall_tests.append(recall_test)
#     f1_tests.append(f1_test)
# print("Test set results:")
# print("accuracy: %0.4f (+/- %0.4f)" % (np.mean(acc_tests), np.std(acc_tests)))
# print("precision: %0.4f (+/- %0.4f)" % (np.mean(precision_tests), np.std(precision_tests)))
# print("recall: %0.4f (+/- %0.4f)" % (np.mean(recall_tests), np.std(recall_tests)))
# print("f1: %0.4f (+/- %0.4f)" % (np.mean(f1_tests), np.std(f1_tests)))