# -*- coding: utf-8 -*-
# @Time : 2023/4/14 9:51
# @Author : Su Hao
# @File : GCN_RF.py
# @Software : PyCharm
import numpy as np
import scipy.sparse as sp
import torch
import pandas as pd
from sklearn.model_selection import cross_val_score
import time
from time import perf_counter
import torch.nn.functional as func
from sklearn.ensemble import RandomForestClassifier


def normalize(mx):
    """Row-normalize sparse matrix"""
    rowsum = np.array(mx.sum(1))  # 对每一行求和
    r_inv = np.power(rowsum, -1).flatten()  # 求倒数
    r_inv[np.isinf(r_inv)] = 0.  # 如果某一行全为0，则r_inv算出来会等于无穷大，将这些行的r_inv置为0
    r_mat_inv = sp.diags(r_inv)  # 构建对角元素为r_inv的对角矩阵
    mx = r_mat_inv.dot(mx)
    # 用对角矩阵与原始矩阵的点积起到标准化的作用，原始矩阵中每一行元素都会与对应的r_inv相乘，最终相当于除以了sum
    return mx


def load_data(feature_path, adj_matrix_path):

    '''分别是流的编号；流的特征，流的的类别，black或者white'''

    idx_features_labels = pd.read_csv(feature_path)

    # 提取出特征和标签
    features = sp.csr_matrix(idx_features_labels[['fwd_pkts_payload.max', 'flow_pkts_payload.avg', 'down_up_ratio', 'fwd_pkts_tot', 'fwd_pkts_payload.tot', 'flow_pkts_per_sec']], dtype=np.float32)

    labels = encode_onehot(idx_features_labels['Label'])
    # labels = idx_features_labels['label'].replace(['black', 'white'], [1, 0])

    # 建图
    # cites file的每一行格式为：<flow ID> <relative_flow_ID>
    # 根据前面的contents与这里的cites创建图，算出edges（度）矩阵与adj（邻接）矩阵
    # 取出流的id
    idxs = np.array(idx_features_labels['index'], dtype=np.int32)

    # idx_map = new_index : origin_index
    # 其中origin_idx是原始文件中的流编号，new_idx是当前文件的流编号
    idx_map = {origin_idx: new_idx for new_idx, origin_idx in enumerate(idxs)}
    # return idx_map

    edges_unordered = pd.read_csv(adj_matrix_path)
    # print('边数：{}'.format(edges_unordered.shape))
    edges_unordered = np.array(edges_unordered.values, dtype=np.int32)
    # edges_unordered为直接从边表文件中直接读取的结果，是一个（edge_num,2)的数组，每一行标识一条边两个端点的idx
    edges = np.array(list(map(idx_map.get, edges_unordered.flatten())), dtype=np.int32).reshape(edges_unordered.shape)
    # edges_unoedered中存储的是端点id，要将每一项的id换成编号
    adj = sp.coo_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])),
                        shape=(labels.shape[0], labels.shape[0]),
                        dtype=np.float32)

    # build symmetric adjacency matrix  论文里A^=(D~)^0.5 A~ (D~)^0.5这个公式
    adj = adj+adj.T.multiply(adj.T > adj)-adj.multiply(adj.multiply(adj.T > adj))
    # 对于无向图,邻接矩阵是对称.上一步得到的adj是按有向图构建的,转换为无向图的邻接矩阵需要扩充成对称矩阵
    # eye创建单位矩阵,第一个参数为行数,第二个为列数
    adj = normalize(adj+sp.eye(adj.shape[0]))

    features = torch.FloatTensor(np.array(features.todense()))  # tensor为pytorch常用的数据结构
    labels = torch.LongTensor(np.where(labels)[1])
    # labels = torch.LongTensor(labels)
    # 这里将onthot label转回index
    adj = sparse_mx_to_torch_sparse_tensor(adj)   # 邻接矩阵转为tensor处理

    return adj, features, labels


def encode_onehot(labels):
    classes = set(labels)  # set() 函数创建一个无序不重复元素集
    classes_dict = {c: np.identity(len(classes))[i, :] for i, c in  # identity创建方矩阵
                    enumerate(classes)}     # 字典 key为label的值，value为矩阵的每一行
    # enumerate函数用于将一个可遍历的数据对象组合为一个索引序列
    labels_onehot = np.array(list(map(classes_dict.get, labels)),  # get函数得到字典key对应的value
                             dtype=np.int32)
    return labels_onehot


def sparse_mx_to_torch_sparse_tensor(sparse_mx):    # 把一个sparse matrix转为torch稀疏张量
    """
    numpy中的ndarray转化成pytorch中的tensor : torch.from_numpy()
    pytorch中的tensor转化成numpy中的ndarray : numpy()
    """
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(np.vstack((sparse_mx.row, sparse_mx.col)).astype(np.int64))
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)


def our_precompute(features, adj_matrix, degree):
    t = perf_counter()
    for i in range(degree):
        if i != degree-1:
            temp_features = func.relu(torch.spmm(adj_matrix, features))
            # features = F.dropout(features, 0.5)
        elif i == degree-1:
            result_features = torch.spmm(adj_matrix, temp_features)
    pre_compute_time = perf_counter()-t
    return result_features, pre_compute_time


adj, features, labels = load_data("./data/nodes_features.csv", "./data/edges.csv")

extract_features, precompute_time = our_precompute(features, adj, 2)


X = extract_features
Y = labels

metrics = ['accuracy', 'precision', 'recall', 'f1']
detect_time = []
for scoring in metrics:
    start = time.process_time()
    clf = RandomForestClassifier(n_estimators=1000)
    scores = cross_val_score(clf, X, Y, cv=5, scoring=scoring)  # cv为迭代次数。
    end = time.process_time()
    detect_time.append(end - start)
    # print(scores)
    print(scoring+": %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))
