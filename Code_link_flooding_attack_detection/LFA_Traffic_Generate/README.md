# 链路泛洪攻击流量生成
## 1. 安装Mininet和Wireshark
## 1.1 源码安装（推荐）
```
git clone https://github.com/mininet/mininet
cd mininet
git tag  # list available versions
git checkout -b mininet-2.3.0 2.3.0  # or whatever version you wish to install
cd ..

install.sh -a
```
测试是否安装成功：
```
sudo mn --switch ovsbr --test pingall
```
这个命令会创建包含两台主机和一个交换机的最小拓扑，并测试连通性。

## 1.2 遇到问题与解决
- 使用虚拟机为`Ubuntu 20.04`会遇到`python-pip`错误，解决方法是修改`install.sh`：
```
PYTHON=${PYTHON:-python}
更改为：
PYTHON=${PYTHON:-python3}
```

- 之后安装过程中出现`git clone`错误，解决方案是修改`install.sh`，把所有的`git://`修改为`https://`例如：
```
git clone https://github.com/mininet/openflow
```

## 2. 构建拓扑
```
from mininet.topo import Topo

class MyTopo( Topo ):
    def __init__( self ):
        # Initialize topology
        Topo.__init__( self )
        
        # Add switches
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')
        s5 = self.addSwitch('s5')
        
        # Add hosts
        for h in range(1, 27):
            ip = '10.0.0.%d' % (h)
            hostname = 'h%d' % (h)
            host = self.addHost(hostname, ip=ip)
            
            # Add links between hosts and switches
            if h >= 1 and h <= 5:
                self.addLink(host, s1)
            elif h >= 6 and h <= 10:
                self.addLink(host, s2)
            elif h >= 11 and h <= 15:
                self.addLink(host, s3)
            elif h >= 16 and h <= 20:
                self.addLink(host, s4)
            elif h >= 21 and h <= 26:
                self.addLink(host, s5)
            
        self.addLink(s1, s6)
        self.addLink(s2, s6)
        self.addLink(s3, s6)
        self.addLink(s4, s6)
        self.addLink(s5, s6)
        
topos = { 'mytopo': ( lambda: MyTopo() ) }
```


## 3. 重放流量简单测试
### 3.1 安装tcpreplay
```
sudo apt-get install tcpreplay
```
### 3.2 简单拓扑搭建
搭建最简单的一个网络拓扑，并设置对应IP：
```
sudo mn
py h1.setIP('10.128.0.2')
py h2.setIP('10.0.0.2')
h2 ping h1
```
这里将`h1`设置为服务器，`h2`设置为客户端，因此两个主机对应的端口分别是`s1-eth1`入端口，`s1-eth2`出端口。此外，由于该数据集中mtu比较大，因此需要设置两个端口的mtu大小：
```
s1 ifconfig s1-eth1 mtu 9000
s1 ifconfig s1-eth2 mtu 9000
```
### 3.3 双向流量重放测试
首先将`https-flood.pcap`文件进行预处理，自动区分`client`和`server`。
```
tcpprep -a client -i https-flood.pcap -o test.prep
```
这里可能会提醒，该数据包不完整，因为做过匿名处理，但是可以不用理会。
然后指定双向端口进行重放：
```
sudo tcpreplay --cachefile=test.prep --intf1=s1-eth2 --intf2=s1-eth1 https-flood.pcap
```
在交换机的端口`s1-eth1`和`s1-eth2`分别能够看到两个方向的流量。


## 6. 流量特征提取
### 6.1 zeek使用与安装
首先安装zeek，这里使用源码安装方式。（不推荐，直接使用二进制安装方式）
1. 安装依赖
```
sudo apt-get install cmake make gcc g++ flex libfl-dev bison libpcap-dev libssl-dev python3 python3-dev swig zlib1g-dev
sudo apt-get install python3-git python3-semantic-version
```
2. 记录一个在wsl2上开启代理的方式
```
#!/bin/bash
host_ip=$(cat /etc/resolv.conf |grep "nameserver" |cut -f 2 -d " ")
export http_proxy="http://$host_ip:7890"
```
第一行找到DNS域名服务器地址，然后使用在终端输入第二行的命令，设置代理端口。同时，代理软件需要打开'Allow LAN'设置。该设置只在当前终端有效，关闭则后续无效。
3. 之后进入`zeek`目录然后构建与编译（不推荐，时间太长）：
```
./configure
make
sudo make install
```

这里直接使用二进制`deb`安装方式，直接上官网下载最新版本。官方提供了两个途径，一种是直接下载`.deb`文件，然后用`dpkg`命令安装，另一种是将添加源，然后使用命令安装，命令如下：
```
echo 'deb http://download.opensuse.org/repositories/security:/zeek/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/security:zeek.list
curl -fsSL https://download.opensuse.org/repositories/security:zeek/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/security_zeek.gpg > /dev/null
sudo apt update
sudo apt install zeek-lts
```
安装过程中可能会出现错误，一般是缺少依赖，根据提示进行安装即可。
之后测试安装是否成功：
```
export PATH=/opt/zeek/bin:$PATH
zeek --version
```
每一次使用zeek，都需要`export`一下。

### 6.2 安装zeek-flowmeter
zeek-flowmeter是开源的已经开发好的特征提取脚本，我们直接使用它来对流量进行特征提取。仓库地址为：https://github.com/zeek-flowmeter/zeek-flowmeter
首先找一个地方下载仓库代码：
```
git clone https://github.com/zeek-flowmeter/zeek-flowmeter.git
```
然后手动将包添加到`zeek`的安装目录文件夹。这里其实可以通过`zkg install .`进行安装，但是很遗憾，普通用户没有权限进入`zeek`的安装目录，后来没有尝试，笔者使用的是手动添加，但是可以尝试：
```
su #进入root用户
export PATH=/opt/zeek/bin:$PATH
zkg install .
```
可能会有效。如果无效，参考手动添加步骤，如下：
```
su #进入root用户，因为读取zeek的设置需要root权限
zeekctl config | grep zeekscriptdir
cd <zeekscriptdir>/site
mkdir flowmeter #进入脚本目录，创建文件夹
cp -r zeek-flowmeter/scripts  <zeekscriptdir>/site/flowmeter #将scripts中的两个文件拷贝到flowmeter目录下
```
这里需要注意，一定是`scripts`目录下直接放两个文件`__load__.zeek`和`flowmeter.zeek`文件，否则后续使用时会找不到。

之后修改`local.zeek`文件，该文件就在`site/`目录下，添加下面代码：
```
# load the FlowMeter for execution
@load flowmeter

# redefining the minimal bulk length to 3 packets
redefine FlowMeter::bulk_min_length = 3;

# disable checksum verification
redef ignore_checksums=T;
```

最后，使用以下命令提取特征：
```
zeek flowmeter -r your.pcap 
```
会出现一些列`log`，其中不同日志可以通过`uid`字段连接。