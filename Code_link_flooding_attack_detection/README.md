# Code_link_flooding_attack_detection

## 项目介绍
本项目聚焦于链路泛洪攻击流量检测，链路泛洪攻击（Link Flooding Attack，LFA）是目前最先进的分布式拒绝服务攻击，它通过聚合大量合法、低速的攻击流量来阻塞关键链路，从而阻止目标区域内的服务器向外界提供服务。相较于传统DDoS的多对一攻击模式，LFA采用多对多的间接攻击模式，因此LFA的攻击流量并不汇聚在单个目标服务器上。这导致传统的DDoS攻击防御方案无法感知攻击，因此难以检测和防御LFA。
![链路泛洪攻击示意图](./image/crossfire.png)

本项目主要包含两个部分：基于Mininet的LFA模拟流量生成、基于图神经网络的LFA流量检测
### 基于Mininet的LFA模拟
详情见`LFA_Traffic_Generate/`目录下的文件：
[基于Mininet的LFA模拟](LFA_Traffic_Generate/README.md)

### 基于图神经网络的LFA流量检测
本仓库实现了两种基于图神经网络的攻击流量检测方法：GCN_RF和GAT。将流量建模成流量图，并在具有关联关系的流量之间添加边。最终进行流量聚合和检测。

- `feature_process.py`：进行流量特征预处理，处理zeek输出的各种日志文件
- `build_edges.py`：用于构建流量之间的关联关系
- `GCN_RF.py`：使用GCN模型进行流量特征聚合，并将聚合后的特征输入到下游RF分类器进行分类
- `GAT_xx.py`：使用GAT模型进行训练和测试，聚合后的特征由softmax分类器进行分类


## DGL库安装

DGL是一个用于图神经网络的深度学习库，它提供了一套灵活而高效的工具，用于构建、训练和部署各种类型的图神经网络模型。

相关URL

[torch官方网站](https://pytorch.org/)

[DGL安装指导网站](https://www.dgl.ai/pages/start.html)

[PyG官方文档](https://pytorch-geometric.readthedocs.io/en/latest/index.html)

## CPU安装

若使用CPU安装，则需要安装cpu版本的dgl。

根据页面提示找到对应版本，推荐使用pip安装：

```
# If you have installed dgl-cuXX package, please uninstall it first.
pip install  dgl -f https://data.dgl.ai/wheels/repo.html
pip install  dglgo -f https://data.dgl.ai/wheels-test/repo.html
```

检查安装结果

```
import dgl
```

若无报错，则说明安装成功，若有报错，首先检查一下安装步骤，是否做到版本一致。


[基本使用方法，可以参考官方教程](https://docs.dgl.ai/en/0.9.x/guide_cn/)


### CUDA安装

找到对应cuda版本，进行cuda版dgl安装，这里以CUDA10.2为例：

```
pip install  dgl -f https://data.dgl.ai/wheels/cu102/repo.html
pip install  dglgo -f https://data.dgl.ai/wheels-test/repo.html
```

### 参考文献
1. [Crossfire链路泛洪攻击](./reference/The_Crossfire_Attack.pdf)
1. [基于图神经网络的恶意流量检测方案](./reference/基于图神经网络的加密恶意流量检测方案.pdf)