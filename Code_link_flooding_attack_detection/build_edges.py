# -*- coding: utf-8 -*-
# @Time : 2023/4/15 10:00
# @Author : Su Hao
# @File : build_edges.py
# @Software : PyCharm
import pandas as pd
import time

# 读取 CSV 文件，生成 UID
df = pd.read_csv('./data/nodes_features.csv')
src_ip_port = (df['id_orig_h'].astype(str) + ':' + df['id_orig_p'].astype(str)).tolist()
dst_ip_port = (df['id_resp_h'].astype(str) + ':' + df['id_resp_p'].astype(str)).tolist()
uids = df['uid']

edges1 = []
edges2 = []

start = time.process_time()
for i in range(df.shape[0]):
    for j in range(i+1, df.shape[0]):
        if (set([src_ip_port[i], dst_ip_port[i]]) & set([src_ip_port[j], dst_ip_port[j]])):
            edges1.append(i)
            edges2.append(j)
end = time.process_time()

edges_unordered = pd.DataFrame({'idx': edges1, 'relative_idx': edges2})
edges_unordered.to_csv("./data/edges.csv", index=False)