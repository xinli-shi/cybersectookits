# -*- coding: utf-8 -*-
# @Time : 2023/4/13 11:28
# @Author : Su Hao
# @File : feature_process.py
# @Software : PyCharm
import pandas as pd
import numpy as np


def read_from_log():

    conn_df = pd.read_csv('../data/eta/conn.log', delimiter='\t', header=8,
                      names=['ts', 'uid', 'id_orig_h', 'id_orig_p', 'id_resp_h', 'id_resp_p', 'proto', 'service',
                             'duration', 'orig_bytes', 'resp_bytes', 'conn_state', 'local_orig', 'local_resp',
                             'missed_bytes', 'history', 'orig_pkts', 'orig_ip_bytes', 'resp_pkts', 'resp_ip_bytes',
                             'tunnel_parents'])
    conn_df = conn_df.drop('ts', axis=1)

    ssl_df = pd.read_csv('../data/eta/ssl.log', delimiter='\t', header=8,
                     names=['ts', 'uid', 'id_orig_h', 'id_orig_p', 'id_resp_h', 'id_resp_p', 'version_ssl', 'cipher',
                            'curve', 'server_name', 'resumed', 'last_alert', 'next_protocol', 'established',
                            'ssl_history', 'cert_chain_fps', 'client_cert_chain_fps', 'sni_matches_cert'])
    ssl_df = ssl_df.drop('ts', axis=1)

    http_df = pd.read_csv('../data/eta/http.log', delimiter='\t', header=8,
                      names=['ts', 'uid', 'id_orig_h', 'id_orig_p', 'id_resp_h', 'id_resp_p', 'trans_depth', 'method',
                             'host', 'uri', 'referrer', 'version_http', 'user_agent', 'origin', 'request_body_len',
                             'response_body_len',
                             'status_code', 'status_msg', 'info_code', 'info_msg', 'tags', 'username', 'password',
                             'proxied', 'orig_fuids', 'orig_filenames', 'orig_mime_types', 'resp_fuids',
                             'resp_filenames', 'resp_mime_types'])
    http_df = http_df.drop('ts', axis=1)

    flowmeter_df = pd.read_csv('../data/eta/flowmeter.log', delimiter='\t', header=8,
                           names=['uid', 'flow_duration', 'fwd_pkts_tot', 'bwd_pkts_tot', 'fwd_data_pkts_tot',
                                  'bwd_data_pkts_tot', 'fwd_pkts_per_sec', 'bwd_pkts_per_sec',
                                  'flow_pkts_per_sec', 'down_up_ratio', 'fwd_header_size_tot', 'fwd_header_size_min',
                                  'fwd_header_size_max', 'bwd_header_size_tot', 'bwd_header_size_min',
                                  'bwd_header_size_max', 'flow_FIN_flag_count', 'flow_SYN_flag_count',
                                  'flow_RST_flag_count', 'fwd_PSH_flag_count', 'bwd_PSH_flag_count',
                                  'flow_ACK_flag_count', 'fwd_URG_flag_count',
                                  'bwd_URG_flag_count', 'flow_CWR_flag_count', 'flow_ECE_flag_count',
                                  'fwd_pkts_payload.min', 'fwd_pkts_payload.max', 'fwd_pkts_payload.tot',
                                  'fwd_pkts_payload.avg', 'fwd_pkts_payload.std',
                                  'bwd_pkts_payload.min', 'bwd_pkts_payload.max', 'bwd_pkts_payload.tot',
                                  'bwd_pkts_payload.avg', 'bwd_pkts_payload.std', 'flow_pkts_payload.min',
                                  'flow_pkts_payload.max', 'flow_pkts_payload.tot',
                                  'flow_pkts_payload.avg', 'flow_pkts_payload.std',
                                  'fwd_iat.min', 'fwd_iat.max', 'fwd_iat.tot', 'fwd_iat.avg', 'fwd_iat.std',
                                  'bwd_iat.min', 'bwd_iat.max', 'bwd_iat.tot', 'bwd_iat.avg',
                                  'bwd_iat.std', 'flow_iat.min', 'flow_iat.max', 'flow_iat.tot', 'flow_iat.avg',
                                  'flow_iat.std', 'payload_bytes_per_second', 'fwd_subflow_pkts', 'bwd_subflow_pkts',
                                  'fwd_subflow_bytes',
                                  'bwd_subflow_bytes', 'fwd_bulk_bytes', 'bwd_bulk_bytes', 'fwd_bulk_packets',
                                  'bwd_bulk_packets', 'fwd_bulk_rate', 'bwd_bulk_rate', 'active.min', 'active.max',
                                  'active.tot', 'active.avg',
                                  'active.std', 'idle.min', 'idle.max', 'idle.tot', 'idle.avg',
                                  'idle.std', 'fwd_init_window_size', 'bwd_init_window_size', 'fwd_last_window_size',
                                  'bwd_last_window_size'])

    merged_df1 = pd.merge(conn_df, ssl_df, how='left', on='uid')
    merged_df2 = pd.merge(flowmeter_df, http_df, how='left', on='uid')
    merged_df = pd.merge(merged_df2, merged_df1, how='left', on='uid')
    # merged_df = merged_df.drop_duplicates(subset=['id_orig_h_x', 'id_orig_p_x', 'id_resp_h_x', 'id_resp_p_x'])
    merged_df.to_csv("eta.csv", index=False)


def add_label(src_file_path, label, save_name):
    df = pd.read_csv(src_file_path)
    df['Label'] = label
    df.to_csv(save_name, index=False)


def convert_by_dict(src_file, dest_file):
    from sklearn.feature_extraction import DictVectorizer
    vec = DictVectorizer()

    df = pd.read_csv(src_file)
    cols_to_convert = ['method', 'version_x', 'status_code', 'proto', 'conn_state',
                       'cipher', 'curve', 'resumed', 'next_protocol', 'established', 'ssl_history', 'sni_matches_cert']
    df[cols_to_convert] = df[cols_to_convert].fillna('-')
    vs1 = df[cols_to_convert].to_dict(orient='records')
    vs2 = vec.fit_transform(vs1).toarray()

    for index, name in enumerate(vec.get_feature_names()):
        df[name] = list(map(lambda x: int(x[index]), vs2))
    df = df.fillna(0)
    df['Label'] = "Black"
    # df = df.drop_duplicates(subset=['id_orig_h', 'id_orig_p', 'id_resp_h', 'id_resp_p'])
    df.to_csv(dest_file, index=False)


# read_from_log()
# convert_by_dict("eta.csv", "eta_feature.csv")

