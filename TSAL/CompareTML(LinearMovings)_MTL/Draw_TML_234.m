clear;
clc;
sigma1 = 3;
sigma2 = 3;
c_1='b->';c_2='g-^';c_3='r-s';c_4='c-d';c_5='m--+';
Err1 = zeros(5,18);
figure;
for sigma3 = 1:1:5
    load(['TML2_sigma1=',num2str(sigma1),'_sigma2=',num2str(sigma2),'_sigma3=',num2str(sigma3),'.mat']);
    pcolor=['c_',num2str(sigma3)];
    semilogy(AnchorNum,Err,eval(pcolor),'LineWidth',1.5);
    hold on;
    Err1(sigma3,:)=TraditionalErr1';
end
TraditionalErr1=mean(Err1);
semilogy(AnchorNum1,TraditionalErr1,'k-o','LineWidth',1.5);

box on;
grid on;
xlabel('Number of Anchors','fontsize',16); %设置x轴的标题和字体大小
ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
AX = legend('TML: \sigma_3=1^\circ','TML: \sigma_3=2^\circ', ...
    'TML: \sigma_3=3^\circ','TML: \sigma_3=4^\circ','TML: \sigma_3=5^\circ','MTL', ...
    'Location','northeast');%设置legend位置。

LEG = findobj(AX,'type','text');
% axis([0 10 1 20 ]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);
%%
clear;
clc;
sigma1 = 3;
sigma3 = 3;
c_1='b->';c_2='g-^';c_3='r-s';c_4='c-d';c_5='m--+';
Err1 = zeros(5,18);
figure;
for sigma2 = 1:1:5
    load(['TML3_1_sigma1=',num2str(sigma1),'_sigma2=',num2str(sigma2),'_sigma3=',num2str(sigma3),'.mat']);
    pcolor=['c_',num2str(sigma2)];
    semilogy(AnchorNum,Err,eval(pcolor),'LineWidth',1.5);
    hold on;
    Err1(sigma2,:)=TraditionalErr1';
end
TraditionalErr1=mean(Err1);
semilogy(AnchorNum1,TraditionalErr1,'k-o','LineWidth',1.5);

box on;
grid on;
xlabel('Number of Anchors','fontsize',16); %设置x轴的标题和字体大小
ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
AX = legend('TML: \sigma_2=1 m','TML: \sigma_2=2 m', ...
    'TML: \sigma_2=3 m','TML: \sigma_2=4 m','TML: \sigma_2=5 m','MTL', ...
    'Location','northeast');%设置legend位置。

LEG = findobj(AX,'type','text');
% axis([0 10 1 20 ]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);
%%
%%
clear;
clc;
sigma2 = 3;
sigma3 = 3;
c_1='r->';d_1='r-->';
c_2='g-^';d_2='g--^';
c_3='b-d'; d_3='b--d';
figure;
i=1;
for sigma1 = 1:2:5
    load(['TML4_sigma1=',num2str(sigma1),'_sigma2=',num2str(sigma2),'_sigma3=',num2str(sigma3),'.mat']);
    pcolor=['c_',num2str(i)];
    semilogy(AnchorNum,Err,eval(pcolor),'LineWidth',1.5);
    hold on;
    pcolor=['d_',num2str(i)];
    semilogy(AnchorNum1,TraditionalErr1,eval(pcolor),'LineWidth',1.5);
    i=i+1;
end
box on;
grid on;
xlabel('Number of Anchors','fontsize',16); %设置x轴的标题和字体大小
ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
AX = legend('TML: \sigma_1=1 m','MTL: \sigma_1=1 m', ...
    'TML: \sigma_1=3 m','MTL: \sigma_1=3 m','TML: \sigma_1=5 m','MTL: \sigma_1=5 m', ...
    'Location','northeast');%设置legend位置。

LEG = findobj(AX,'type','text');
% axis([0 10 1 20 ]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);