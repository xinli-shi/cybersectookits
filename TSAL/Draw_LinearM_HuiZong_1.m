% 论文fig7, fig8

clear;clc;
WalkDist1 = 100; % walk 10 meters for the first moving
WalkDist2 = WalkDist1; % walking distance for the second moving

TMDistArrayREAL=[WalkDist1,WalkDist2];
ThetaArrayREAL = [0 90]; % unit: degree
AnchorTCS = [50 300]; %[50 -50]; %[300 300];% anchor coordinate in real coordinate system
TargetTCS = [0 0]; % target coordinate in real coordinate system

sigma1Array =0:2:6; % gaussian ranging error deviation for the anchor's measurement% unit:meter
sigma2Array = 0:2:6; % gaussian error deviation for walking distance %unit:meter
sigma3Array=0:2:6;

for sigma3 = sigma3Array
    % 加载数据，
file=['TMSAL_LinearM_WalkDistReal=',num2str(TMDistArrayREAL),'_TurningAngleReal=',num2str(ThetaArrayREAL), ...
    '_AnchorTCS=',num2str(AnchorTCS),'_TargetTCS=',num2str(TargetTCS),'_sigma1=',num2str(sigma1Array), ...
    '_sigma2=',num2str(sigma2Array),'_sigma3=',num2str(sigma3),'.mat']

% file = ['TMSAL_TriM_WalkDistReal=',num2str(TMDistArrayREAL),'_TurningAngleReal=',num2str(ThetaArrayREAL), ...
%     '_AnchorTCS=',num2str(AnchorTCS),'_TargetTCS=',num2str(TargetTCS),'_sigma1=',num2str(sigma1Array), ...
%     '_sigma2=',num2str(sigma2Array),'_sigma3=',num2str(sigma3),'.mat']

load(file);


%% (ctrl+enter：运行当前节)
figure;
% distFig('Rows',2,'Columns',2)
% barmap=[1 1 1;1 1 1;1 1 1;1 1 1;1 1 1];
barweb(Average,Variance,1,gpname,'','\sigma_1','Localization Error (meters)',barmap,'none',leg,2,'plot','h');
% save('RAW_Results','Average','Variance','gpname','barmap','leg');
% barweb(Average,Variance,1);
hold on;


grid on;
box on;
set(gca,'FontSize',14);
end