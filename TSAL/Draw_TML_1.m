load('TML_sigma1=4, sigma3=2,sigma2=2,4,6,8.mat');
p1 = plot(AnchorNum,Err2,'b->','LineWidth',1.5);
hold on
p2 = plot(AnchorNum,Err4,'g-^','LineWidth',1.5);
p3 = plot(AnchorNum,Err6,'r-s','LineWidth',1.5);
p4 = plot(AnchorNum,Err8,'c-d','LineWidth',1.5);

p5 = plot(AnchorNum1,TraditionalErr1,'k-o','LineWidth',1.5);

box on;
grid on;
xlabel('Number of Anchors','fontsize',16); %设置x轴的标题和字体大小
ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
AX = legend([p1 p2 p3 p4 p5],'\sigma_2=2 m, \sigma_3=2^\circ','\sigma_2=4 m, \sigma_3=2^\circ', ...
    '\sigma_2=6 m, \sigma_3=2^\circ','\sigma_2=8 m, \sigma_3=2^\circ','Multilateration Localization', ...
    'Location','northeast');%设置legend位置。

LEG = findobj(AX,'type','text');
% axis([0 10 0 90]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);

%%
% load('TML_sigma1=4, sigma3=2,sigma2=2,4,6,8.mat');
load('TML_sigma1=4, sigma2=2,sigma3=2,4,6,8.mat');
% 
figure;
p1 = plot(AnchorNum,Err22,'b->','LineWidth',1.5);
hold on
p2 = plot(AnchorNum,Err24,'g-^','LineWidth',1.5);
p3 = plot(AnchorNum,Err26,'r-s','LineWidth',1.5);
p4 = plot(AnchorNum,Err28,'c-d','LineWidth',1.5);
% plot(AnchorNum,Err2,'k->','LineWidth',1.5);
% hold on
% plot(AnchorNum,Err4,'k-^','LineWidth',1.5);
% plot(AnchorNum,Err6,'k-s','LineWidth',1.5);
% plot(AnchorNum,Err8,'k-d','LineWidth',1.5);

p5 = plot(AnchorNum1,TraditionalErr1,'k-o','LineWidth',1.5);

box on;
grid on;
xlabel('Number of Anchors','fontsize',16); %设置x轴的标题和字体大小
ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
AX = legend([p1 p2 p3 p4 p5],'\sigma_2=2 m, \sigma_3=2^\circ','\sigma_2=2 m, \sigma_3=4^\circ', ...
    '\sigma_2=2 m, \sigma_3=6^\circ','\sigma_2=2 m, \sigma_3=8^\circ','Multilateration Localization', ...
    'Location','northeast');%设置legend位置。

LEG = findobj(AX,'type','text');
% axis([0 10 0 90]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);