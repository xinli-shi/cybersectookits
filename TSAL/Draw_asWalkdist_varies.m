%论文fig11， 移动距离对于两种方法的影响
clear;clc;
figure;
load('LinearM_TriM_WalkDist_Results_AllThreeSigmas=2');%加载数据
leg={'TML-SAL based on Two Linear Movings','TML-SAL based on A Triangular Moving'}; %legend即图例
barweb(Average,Variance,1,gpname,'','Moving Distance (m)','Localization Error (m)',barmap,'none',leg,2,'plot','v');
hold on;
grid on;
box on;
set(gca,'FontSize',14);