function [TargetEstimation] = ExistingLocAlgorithm(Anchor,Distance)
% existing localization algorithm using multilateration methods; here we
% need to make sure that there are at least three anchor nodes.
%   Detailed explanation goes here
% input:
%       Anchor: m*2 matrix, the locations of the anchor nodes
%       Distance: M*NN matrix, each anchor ndoe has NN measurements
% output:
%       TargetEstimation: 1*2 vector, the location of the target node
%
% Guanghui Wang, October 8, 2018

%% setup
[m,~] = size(Anchor);
[m1,~] = size(Distance);

if m < 3
    error('Error! There should be at least three anchor nodes!');
end

if m ~= m1
    error('Error! Each anchor should have its measurements!');
end


%% the main part
% get the distance information
Dis = zeros(m,1);

for i = 1:m
    d = Distance(i,:);
    Dis(i,1) = mean(d);
end


%% calculate the estimated location of target
% construct the matrix A and matrix B according to their definitions;
A=zeros(m-1,2);  
B=zeros(m-1,1);
for i = 1:m-1 % construct matrix A and B
    A(i,1) = 2*(Anchor(m,1)-Anchor(i,1));
    A(i,2) = 2*(Anchor(m,2)-Anchor(i,2));
    B(i,1) = Anchor(m,1)^2 - Anchor(i,1)^2 + Anchor(m,2)^2 - Anchor(i,2)^2 + Dis(i,1)^2 - Dis(m,1)^2;
end
% using matrix A and matrix B, calcualte the coordinate of the target
TargetEstimation = (inv(A'*A))*A'*B; 
TargetEstimation = TargetEstimation';

end

