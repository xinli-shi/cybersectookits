function [Distance,Angle] = GetMeasurementForEachAnchor(Anchor, Target, sigmaDistance, sigmaAngle, N)
% simulate the measurements for each anchor, the measurements include the
% distance measurements and the angle measurements
%   Detailed explanation goes here
% Input:
%      Anchor: 1*2 vector, dentoe the location of the anchor
%      Target: 1*2 vector, denote the lcoation of the target
%      sigma1: the standard deviation of the simulated distance
%      sigma2: the standard deviation of the simulated angle
%      N: the number of the measurements
% output:
%      Distance: N*1 vector, the distance measurements
%      Angle: N*1 vector, the angle measurements

%% setup
x1 = Anchor(1,1); y1 = Anchor(1,2); % the location of the anchor node
x = Target(1,1); y = Target(1,2); % the location of the target node

sigma1 = sigmaDistance;
sigma2 = sigmaAngle;

Distance = zeros(N,1);
Angle = zeros(N,1);

%% get the simulated measurements

for i =1:N
    % get the distance measurement
    temp = sqrt((x1-x)^2 + (y1-y)^2);
    RE = randn(1,1)*sigma1;
    Distance(i,1) = temp + RE;
    
    % get the angle measurement
    
    if x >= x1 && y >= y1
        alpha0 = atan((y - y1)/(x - x1));
        %Alpha = alpha0 + sigma2*randn(1,1);
    elseif x < x1 && y > y1
        alpha0 = atan((y - y1)/(x - x1)) + pi;
    elseif x < x1 && y < y1
        alpha0 = atan((y - y1)/(x - x1)) + pi;
    elseif x > x1 && y < y1
        alpha0 = atan((y - y1)/(x - x1)) + 2*pi;
    end
    
    Angle(i,1) = alpha0 + sigma2*randn(1,1);

end

