function [TargetEstimated] = ProposedLocAlgorithm(Anchor, Distance, Angle)
% the proposed localization algorithm using single anchor localization
%   Detailed explanation goes here
% input:
%      Anchor: m*2 matrix, the location of the anchor nodes, the number of
%      the anchor nodes is m
%      Distance: m*N matrix, the distance measurements, for each anchor
%      node, the number of the measurements is N
%      Angle: m*N matrix, the angle measurements, for each anchor node, the
%      number of the measurements is N
% output:
%      TargetEstimated: 1*2 vector, the location of the target node;
%
% Guanghui Wang, October 8, 2018

%% setup
[m,~] = size(Anchor); % the number of the anchor nodes is m;
[m1,N] = size(Distance); % the number of the measurements is N;
[m2,N1] = size(Angle);

% test the setup
if m1 ~= m
    error('Error! Something is wrong on the distance ranging with the number of anchor ndoes!');
end

if m2 ~= m
    error('Error! Something is wrong on the angle ranging with the number of anchor node!');
end

if N ~= N1
    error('Error! The numbers of the distance measurements and the angle measurements should be equal!');
end

%% the main part
Target = zeros(m,2); % each anchor will have an estimation

for i = 1:m
    % first get the anchor and its measurements
    Anchor0 = Anchor(i,:);
    Distance0 = Distance(i,:);
    Angle0 = Angle(i,:);

    % get the mean of the measurements
    d = mean(Distance0); a = mean(Angle0);
    % get the estimated result for anchor0
    Target(i,:) = SingleAnchorLocModel(a, d, Anchor0);
end

x = mean(Target(:,1));
y = mean(Target(:,2));
%TargetEstimated = mean(Target);
TargetEstimated = [x,y];

end

