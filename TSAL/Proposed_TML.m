function [EstimatedTarget, Err] = Proposed_TML(AnchorTCSArray,TargetTCS, ...
                            ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma31,sigma32,N)
%%  作者：童飞
%   日期：2020-05-19
%% 函数说明：
%       本函数用于实现TML算法，算法调用TM-SAL based on triangular moving算法，或TM-SAL based
%       on two linear movings算法
%% 返回值说明：
%   EstimatedTarget: 目标节点的估计位置
%   Err: 目标节点估计位置与真实位置的距离
%% 参数说明：
%   缩写说明: TCS: True Cartesian coordinate system
%             VCS: Virtual Cartesian coordinate system
%   AnchorTCS: the true coordinate of the anchor node in TCS
%   TargetTCS: the true coordinate of the target node at the original location A in TCS
%   ThetaArrayREAL: ThetaArrayREAL(1): the real counterclockwise angle from the positive abscissa axis of TCS to that of VCS
%                   ThetaArrayREAL(2): the real clockwise angle from the previous direction to the new one measured by the target node when it stops at location B
%                                      theat2不能是0度（表示target在B没有转向）或180度（表示target在B转向相反方向）。
%                   注意：这两个angle都是角度表示（不是弧度）。ThetaArrayREAL(2)角度不参与计算，只是用来判定target节点的左右转向
%   TMDistArrayREAL: TM表示Target Moving,
%                    TMDistArrayREAL(1): the first real moving distance of the target node
%                    TMDistArrayREAL(2): the second real moving distance of the target node
%   sigma1:  a standard deviation with respect to the distance measured by 
%            an anchor node from itself to the target node
%   sigma2:  a standard deviation with respect to the moving distance 
%            measured by the target node 
%   sigma31: （以角度为单位）a standard deviation with respect to \theta_1, the 
%            counterclockwise angle from the positive abscissa axis of TCS to that of VCS
%   sigma32: 如果为负值，表示采用TM-SAL based on triangular moving
%              方法；如果为>=0的值，表示采用TM-SAL based on two linear movings 方法,其具体为
%              （以角度为单位）a standard deviation with respect to \theta_2, the clockwise
%            angle from the previous direction to the new one measured by the target
%            node when it stops at location B

% theat2 不能是0度（表示target在B没有转向）或180度（表示target在B转向相反方向）
if ThetaArrayREAL(2) == 0 || ThetaArrayREAL(2) == 180
    error 'Theta_2 cannot be 0 or 180 degrees.'
end
AnchorNum = size(AnchorTCSArray,1);
EstimatedTargetArray = zeros(AnchorNum,2);

if sigma32 < 0 % 如果为负值，表示采用TM-SAL based on triangular moving方法
    for i=1:AnchorNum
        [EstimatedTarget, ~] = Proposed_TMSAL_TriangularMoving(AnchorTCSArray(i,:),TargetTCS, ...
                            ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma31,N);
        EstimatedTargetArray(i,:) = EstimatedTarget;
    end
else % 如果为>=0的值，表示采用TM-SAL based on two linear movings 方法
    for i=1:AnchorNum
        [EstimatedTarget, ~] = Proposed_TMSAL_TwoLinearMovings(AnchorTCSArray(i,:),TargetTCS, ...
                            ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma31,sigma32,N);
        EstimatedTargetArray(i,:) = EstimatedTarget;
    end
end

x = mean(EstimatedTargetArray(:,1));
y = mean(EstimatedTargetArray(:,2));
EstimatedTarget = [x y];
Err = norm(TargetTCS-EstimatedTarget);

end

