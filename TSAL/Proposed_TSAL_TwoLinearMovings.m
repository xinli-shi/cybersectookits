function [EstimatedTarget, Err] = Proposed_TMSAL_TwoLinearMovings(AnchorTCS,TargetTCS, ...
                            ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma31,sigma32,N)
%%  作者：童飞
%   日期：2020-05-18
%% 函数说明：
%       本函数用于实现TM-SAL based on two linear
%       movings算法，用于确定Target的初始位置坐标。在该算法中，target
%       初始位于A点，沿着与真实坐标系的x轴逆时针呈theta_1角度的方向移动到B点，
%       在顺时针旋转theta_2，移动到C点，至此即可判断出A的坐标
%% 返回值说明：
%   EstimatedTarget: 目标节点的估计位置
%   Err: 目标节点估计位置与真实位置的距离
%% 参数说明：
%   缩写说明: TCS: True Cartesian coordinate system
%             VCS: Virtual Cartesian coordinate system
%   AnchorTCS: the true coordinate of the anchor node in TCS
%   TargetTCS: the true coordinate of the target node at the original location A in TCS
%   ThetaArrayREAL: ThetaArrayREAL(1): the real counterclockwise angle from the positive abscissa axis of TCS to that of VCS
%                   ThetaArrayREAL(2): the real clockwise angle from the previous direction to the new one measured by the target node when it stops at location B
%                                      theat2 不能是0度（表示target在B没有转向）或180度（表示target在B转向相反方向）
%                   注意：这两个angle都是角度表示（不是弧度）
%   TMDistArrayREAL: TM表示Target Moving,
%                    TMDistArrayREAL(1): the first real moving distance of the target node
%                    TMDistArrayREAL(2): the second real moving distance of the target node
%   sigma1:  a standard deviation with respect to the distance measured by 
%            an anchor node from itself to the target node
%   sigma2:  a standard deviation with respect to the moving distance 
%            measured by the target node 
%   sigma31: （以角度为单位）a standard deviation with respect to \theta_1, the 
%            counterclockwise angle from the positive abscissa axis of TCS to that of VCS
%   sigma32: （以角度为单位）a standard deviation with respect to \theta_2, the clockwise
%            angle from the previous direction to the new one measured by the target
%            node when it stops at location B
%   N: 重复N次计算，最后的结果是这N次计算结果的平均值

% theat2 不能是0度（表示target在B没有转向）或180度（表示target在B转向相反方向）
if ThetaArrayREAL(2) == 0 || ThetaArrayREAL(2) == 180
    error 'Theta_2 cannot be 0 or 180 degrees.'
end
digits(64); % 计算精度提升为64位，默认是32位
format long;
I=AnchorTCS(1); J=AnchorTCS(2);
EstimatedTargetArray = zeros(N,2);

for i=1:N % 进行N次计算
    %% 角度转换成弧度，同时计算出测量值
    theta1 = (ThetaArrayREAL(1) + sigma31*randn(1,1))*pi/180;
    theta2 = (ThetaArrayREAL(2) + sigma32*randn(1,1))*pi/180;
    
    %% 计算测量值 d1,d2,d4
    d1 = norm(AnchorTCS-TargetTCS) + sigma1*randn(1,1);
    d2 = TMDistArrayREAL(1) + sigma2*randn(1,1);
    d4 = TMDistArrayREAL(2) + sigma2*randn(1,1);
    
    %% d3,d5的计算依赖 d1,d2,d4,theta1, theta2的真实值
    %% (realBx,realBy) is the location of B in TCS
    realBx = TargetTCS(1)+TMDistArrayREAL(1)*cos(ThetaArrayREAL(1)*pi/180);
    realBy = TargetTCS(2)+TMDistArrayREAL(1)*sin(ThetaArrayREAL(1)*pi/180);
    d3 = norm(AnchorTCS-[realBx realBy]) + sigma1*randn(1,1);
    %% (realCx,realCy) is the location of C in TCS
    if ThetaArrayREAL(2) <= ThetaArrayREAL(1)
        realCx = realBx+TMDistArrayREAL(2)*cos((ThetaArrayREAL(1)-ThetaArrayREAL(2))*pi/180);
        realCy = realBy+TMDistArrayREAL(2)*sin((ThetaArrayREAL(1)-ThetaArrayREAL(2))*pi/180);
    else %ThetaArrayREAL(2) > ThetaArrayREAL(1)
        realCx = realBx+TMDistArrayREAL(2)*cos((ThetaArrayREAL(1)-ThetaArrayREAL(2))*pi/180+2*pi);
        realCy = realBy+TMDistArrayREAL(2)*sin((ThetaArrayREAL(1)-ThetaArrayREAL(2))*pi/180+2*pi);
    end
    d5 = norm(AnchorTCS-[realCx realCy]) + sigma1*randn(1,1);
    
    %% 计算Target的估计位置：EstimatedTarget
    % a = I*cos(theta1) + J*sin(theta1);
    % b = J*cos(theta1) - I*sin(theta1);
    x = (d1^2-d3^2+d2^2)/(2*d2);
    y = (d5^2-d3^2-d4^2+2*(x-d2)*d4*cos(theta2))/(2*d4*sin(theta2));
    X = I-x*cos(theta1)+y*sin(theta1);
    Y = J-y*cos(theta1)-x*sin(theta1);
    EstimatedTarget = [X Y];
    EstimatedTargetArray(i,:)=EstimatedTarget;
%     Err = norm(EstimatedTarget-TargetTCS);
end
x = mean(EstimatedTargetArray(:,1));
y = mean(EstimatedTargetArray(:,2));
EstimatedTarget = [x y];
Err = norm(TargetTCS-EstimatedTarget);

end

