# TSAL: Target-movement-based Single-Anchor Localization

## 介绍

本项目为基于节点移动的单锚点定位方案。

传统的定位方法需要有多个锚节点参与定位，例如基于距离测量的定位要在n维空间中实现对目标节点的定位需要至少n+1个锚节点。单锚点定位(single-anchor localization, SAL)可以降低成本、简化系统设计实现，具有更高的灵活性和更好的可扩展性，但先前的SAL方案大多要求锚节点配备天线阵列以同时测量距离信息和角度信息，或者对网络拓扑有特殊的要求。

我们提出了一种基于目标节点移动的单锚点定位方法TSAL。包括两个子方案，第一个子方案基于两次线性移动，要求目标节点测量其移动距离和转向角度。第二个子方案基于一次三角形移动，仅要求目标节点测量其移动距离。此外，TSAL还可以与多个锚定节点一起使用，可以获得更准确的定位结果，即TML方案。更详细的介绍可以参考我们的TMC论文：
> F. Tong, B. Ding, Y. Zhang, S. He and Y. Peng, "A Single-Anchor Mobile Localization Scheme," in IEEE Transactions on Mobile Computing, 2022, doi: 10.1109/TMC.2022.3221957.

## 代码说明

定位方案被实现为matlab函数：

- `Proposed_TSAL_TwoLinearMovings.m` 基于两次线性移动的子方案
- `Proposed_TSAL_TriangularMoving.m` 基于一次三角形移动的子方案
- `Proposed_TML.m ` 基于TSAL的多锚点定位方案

`main_*.m` 文件调用上述三个函数，探究不同参数对定位性能的影响