function [Target] = SingleAnchorLocModel(Alpha, Distance, Anchor)
%Single anchor localization model; using the distance and angle information
%to calculate the target location
%   Detailed explanation goes here

%% setup
d = Distance;
a = Alpha;

x1 = Anchor(1,1); 
y1 = Anchor(1,2);

%x=0;
%y=0;

%% calculate the location of the target;
%beta1 = d/sqrt(1+tan(a)^2);
%beta2 = (d*tan(a))/sqrt(1+tan(a)^2);

%if a >= 0 && a < pi/2
%    x = x1 + beta1;
%    y = y1 + beta2;
%elseif a >= pi/2 && a < (3*pi)/2
%    x = x1 - beta1;
%    y = y1 - beta2;
%elseif a >= (3*pi)/2 && a < 2*pi
%    x = x1 + beta1;
%    y = y1 + beta2;
%end

x = x1 + d*cos(a);
y = y1 + d*sin(a);

Target(1,1) = x;
Target(1,2) = y;

end

