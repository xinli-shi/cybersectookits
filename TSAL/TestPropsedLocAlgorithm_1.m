
clear;
clc;
%% setup

% we also need to randomly choose the anchor nodes
%load anchor; 
%Anchor = anchor;
%m = length(Anchor); % number of anchors;
m = 20;
Target = [200, 200]; % target coordinate;

% gaussian ranging error deviation
sigma1 = 4; % 不变
sigma2 = 2;
sigma3 = 8;
%sigmaAngle = 0.0175; % around 1 degree
sigmaAngle = 0.0349; % around 2 degree
%sigmaAngle = 0.0524; % around 3 degree
%sigmaAngle = 0.0698; % around 4 degree
%sigmaAngle = 0.0873; % around 5 degree
%sigmaAngle = 0;

N = 100; % the number of the measurements
NN = 1000; % the number of runs to reduce the randomness
%% test the algorithm with the increment of the number of anchor nodes
% anchor number: 1 -- 20


Error = ones(m,NN);
TraditionalError1 = ones(m-2,NN);
tic
for i = 1:NN % for each run
    LocationEstimation = zeros(m,2);
    ErrorProposedAlg = ones(m,1); 
    
    LocationEstimation1 = zeros(m,2);
    ErrorProposedAlg1 = ones(m,1); 

    Anchor = rand(m,2)*500;
    
    % first get the measurement for each anchor node
    Distance = zeros(m,N);
    Angle = zeros(m,N);
    for j = 1:m
        % for each anchor node
        Anchor0 = Anchor(j,:);
        [Distance0,Angle0] = GetMeasurementForEachAnchor(Anchor0, Target, sigma1, sigmaAngle, N);
        Distance(j,:) = Distance0';
        Angle(j,:) = Angle0';
    end % now the measurements have been obtained
    % next, calculate the localization result using the proposed algorithm
    for j = 1:m
        CurrentAnchor = Anchor(1:j,:);
        CurrentDistance = Distance(1:j,:);
        CurrentAngle = Angle(1:j,:);
        % calcluate the proposed algorithm
        [TargetTemp, ErrorProposedAlg(j,1)] = Proposed_TML(CurrentAnchor,Target, ...
                            [0 270],[100 100],sigma1,sigma2,sigma3,sigma3,1000);
        
        % calculate the existing algorithm
        if j >= 3
            [TargetTemp1] = ExistingLocAlgorithm(CurrentAnchor,CurrentDistance);
            LocationEstimation1(j,:) = TargetTemp1;
            ErrorProposedAlg1(j,1) = sqrt((Target(1,1) - TargetTemp1(1,1))^2 + (Target(1,2) - TargetTemp1(1,2))^2);
        end
    end    
   % next how to record the results for 1000 runs; we can only record the
   % errors 
   Error(:,i) = ErrorProposedAlg(:,1);
   TraditionalError1(:,i) = ErrorProposedAlg1(3:end,1);
end
toc
%% proposed Target Movement based Localization
% N=10000
% simN=1000
% ErrIteration=zeros(1,simN);
% Error = zeros(1,m);
% for i=1:m % for each anchor
%     CurrentAnchor = Anchor(1:i,:);
%     for k=1:simN
%         %[ErrIteration(k)] = MainSALRAW(Anchor,Target,TargetWalkDist,sigmaMeasureDist,sigmaWalkDist,N);
%         [TargetTemp, ErrIteration(k)] = Proposed_TML(CurrentAnchor,Target, ...
%                             [0 90],[100 100],sigmaDistance,sigmaDistance,2,2,N);
%     end
%     Error(i) = mean(ErrIteration);
% end

%% plot the error with the number of anchor nodes
AnchorNum = 1:m;
AnchorNum1 = 3:m;

Error = Error';
Err28 = mean(Error); %数字与sigma2对应

TraditionalError1 = TraditionalError1';
TraditionalErr1 = mean(TraditionalError1);

%%
figure(1);
plot(AnchorNum,Err28,'r-o','MarkerSize',8,'LineWidth',1.5)
hold on
plot(AnchorNum1,TraditionalErr1,'b-*','MarkerSize',8,'LineWidth',1.5)

xlim([1,20]);
set(gca,'xtick',1:1:20);
set(gca,'YScale','log');
legend('\fontsize{14} ADL','\fontsize{14} TML')
xlabel({'Number of anchors'},'FontSize',10);
ylabel({'Location estimation error (meters)'},'FontSize',10);
hold off

% plot the running time
% 
% RT = RT';
% RTT = mean(RT);
% 
% RT1 = RT1';
% RTT1 = mean(RT1);

% figure(2);
% plot(AnchorNum,RTT,'r-o','MarkerSize',8,'LineWidth',1.5)
% hold on
% %plot(AnchorNum1,RTT1,'b-*','MarkerSize',8,'LineWidth',1.5)
% 
% xlim([1,20]);
% set(gca,'xtick',1:1:20);
% set(gca,'YScale','log');
% legend('\fontsize{14} ADL','\fontsize{14} TML')
% xlabel({'Number of anchors'},'FontSize',10);
% ylabel({'Running time (seconds)'},'FontSize',10);
% hold off

