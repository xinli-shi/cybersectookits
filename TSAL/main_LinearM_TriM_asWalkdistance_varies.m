clear;clc;
% �ƶ������Ӱ��
WalkDistArray = 20:20:100; % walk 10 meters for the first moving
ThetaArrayREAL = [0 90]; % unit: degree

AnchorTCS = [50, 300]; % anchor coordinate in real coordinate system
TargetTCS = [0,0]; % target coordinate in real coordinate system

sigma1 =2; % gaussian ranging error deviation for the anchor's measurement% unit:meter
sigma2 = 2; % gaussian error deviation for walking distance %unit:meter
sigma3 = 6;

N=10000;
simN=1000;
WD_N=size(WalkDistArray,2);

ErrIteration=zeros(simN,2);
Average = zeros(WD_N,2);
Variance = zeros(WD_N,2);
i=0;
for WalkDist1 = WalkDistArray
    i=i+1
    for k=1:simN
        WalkDist2 = WalkDist1; % walking distance for the second moving
        TMDistArrayREAL=[WalkDist1,WalkDist2];
%         TargetWalkDist=[WalkDist1,WalkDist2];
%         [ErrIteration(k,1)] = MainSALRAW(Anchor,Target,TargetWalkDist,sigmaMeasureDist,sigmaWalkDist,N);
        [~, ErrIteration(k,1)] = Proposed_TMSAL_TwoLinearMovings(AnchorTCS,TargetTCS, ...
                ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma3,sigma3,N);
            
            
%         WalkDist2 = WalkDist1; % walking distance for the second moving
%         WalkDist3 = WalkDist1*sqrt(2);
%         TargetWalkDist=[WalkDist1,WalkDist2,WalkDist3];
%         [ErrIteration(k,2)] = MainSALTRIW(Anchor,Target,TargetWalkDist,sigmaMeasureDist,sigmaWalkDist,N);
        [~, ErrIteration(k,2)] = Proposed_TMSAL_TriangularMoving(AnchorTCS,TargetTCS, ...
                ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma3,N);
    end
    Average(i,1) = mean(ErrIteration(:,1));
    Variance(i,1) = var(ErrIteration(:,1));
    
    Average(i,2) = mean(ErrIteration(:,2));
    Variance(i,2) = var(ErrIteration(:,2));
end

leg={'TML-SAL based on Two Linear Movings','TML-SAL based on A Triangular Moving'}; %legend��ͼ��
% leg={'Rectangular Moving','Triangular Moving'}; %legend��ͼ��
gpname={'20','40','60','80','100'}; %legend��ͼ��
% gpname = {['0 m'],['2 m'],['3 m'], ['4 m'],['5 m']}';
% gpname = {['upload image'],['download image'],['download pdf'],['upload pdf']} %��ͬ�����ݵ�����
barmap=[1 1 1;1 0 0];
% barmap=[1 1 1;1 1 1;1 1 1;1 1 1;1 1 1];
barweb(Average,Variance,1,gpname,'','Moving Distance (m)','Localization Error (m)',barmap,'none',leg,2,'plot','v');
hold on;
grid on;
box on;
set(gca,'FontSize',14);
hold on;
