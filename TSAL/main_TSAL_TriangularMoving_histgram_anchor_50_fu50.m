clear;clc;
WalkDist1 = 100; % walk 10 meters for the first moving
WalkDist2 = WalkDist1; % walking distance for the second moving

TMDistArrayREAL=[WalkDist1,WalkDist2];
ThetaArrayREAL = [0 270]; % unit: degree
AnchorTCS = [50 -50]; % anchor coordinate in real coordinate system
TargetTCS = [0 0]; % target coordinate in real coordinate system

sigma1Array =0:2:6; % gaussian ranging error deviation for the anchor's measurement% unit:meter
sigma2Array = 0:2:6; % gaussian error deviation for walking distance %unit:meter
% sigma3Array = [0 0];%2:2:4; % gaussion error deviation for target turning angle; unit: degree
for sigma3 = 0:2:6
N=10000
simN=1000
sigma1_N=size(sigma1Array,2);
sigma2_N=size(sigma2Array,2);
% sigma3_N=size(sigma3Array,2);
ErrIteration=zeros(1,simN);
Average = zeros(sigma1_N,sigma2_N);
Variance = zeros(sigma1_N,sigma2_N);
i=0;

tic
for sigma1 = sigma1Array
    i=i+1
    j=0;
    for sigma2 = sigma2Array
        j=j+1
        for k=1:simN
            %[ErrIteration(k)] = MainSALRAW(Anchor,Target,TargetWalkDist,sigmaMeasureDist,sigmaWalkDist,N);
            [EstimatedTarget, ErrIteration(k)] = Proposed_TMSAL_TriangularMoving(AnchorTCS,TargetTCS, ...
                ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma3,N);
            %[EstimatedTarget, ErrIteration(k)] = Proposed_TMSAL_TwoLinearMovings(AnchorTCS,TargetTCS, ...
            %   ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma3,sigma3,N);
        end
        Average(i,j) = mean(ErrIteration);
        Variance(i,j) = var(ErrIteration);
    end
    %     errorbar(sigma1Array,Average(i,:),Variance(i,:));
end
toc

leg={['\sigma_2=',num2str(sigma2Array(1)),' m'],['\sigma_2=',num2str(sigma2Array(2)),' m'], ...
    ['\sigma_2=',num2str(sigma2Array(3)),' m'],['\sigma_2=',num2str(sigma2Array(4)),' m']}; %legend即图例
gpname={[num2str(sigma1Array(1)),' m'],[num2str(sigma1Array(2)),' m'],[num2str(sigma1Array(3)),' m'],[num2str(sigma1Array(4)),' m']}; %legend即图例
% gpname = {['0 m'],['2 m'],['3 m'], ['4 m'],['5 m']}';
% gpname = {['upload image'],['download image'],['download pdf'],['upload pdf']} %不同组数据的名字
barmap=[1 1 1;0 0 1;0 1 1;1 0 0;1 1 0];
% barmap=[1 1 1;1 1 1;1 1 1;1 1 1;1 1 1];

save(['TMSAL_TriM_WalkDistReal=',num2str(TMDistArrayREAL),'_TurningAngleReal=',num2str(ThetaArrayREAL), ...
    '_AnchorTCS=',num2str(AnchorTCS),'_TargetTCS=',num2str(TargetTCS),'_sigma1=',num2str(sigma1Array), ...
    '_sigma2=',num2str(sigma2Array),'_sigma3=',num2str(sigma3),'.mat'],'sigma1Array','sigma2Array','Average','Variance','leg','gpname','barmap');

%% (ctrl+enter：运行当前节)
figure;
barweb(Average,Variance,1,gpname,'','\sigma_1','Localization Error (meters)',barmap,'none',leg,2,'plot','h');
% save('RAW_Results','Average','Variance','gpname','barmap','leg');
% barweb(Average,Variance,1);
hold on;

grid on;
box on;
end
% % l_sigma23_11 = 'k-s';%表示\sigma2和\sigma3都取数组的第一个值时所用的颜色和线型
% % l_sigma23_12 = 'k--s';
% % l_sigma23_21 = 'b->';
% % l_sigma23_22 = 'b-->';
% % linewidth_value=1.5;
% % 
% % errorbar(sigma1Array,Average(:,1),Variance(:,1),l_sigma23_11,'LineWidth',linewidth_value);hold on;
% % sigma23_11 = plot(sigma1Array,Average(:,1),l_sigma23_11,'LineWidth',linewidth_value);
% % 
% % errorbar(sigma1Array,Average(:,2),Variance(:,2),l_sigma23_12,'LineWidth',linewidth_value);
% % sigma23_12 = plot(sigma1Array,Average(:,2),l_sigma23_12,'LineWidth',linewidth_value);
% % 
% % errorbar(sigma1Array,Average(:,3),Variance(:,3),l_sigma23_21,'LineWidth',linewidth_value);
% % sigma23_21 = plot(sigma1Array,Average(:,3),l_sigma23_21,'LineWidth',linewidth_value);
% % 
% % errorbar(sigma1Array,Average(:,4),Variance(:,4),l_sigma23_22,'LineWidth',linewidth_value);
% % sigma23_22 = plot(sigma1Array,Average(:,4),l_sigma23_22,'LineWidth',linewidth_value);
% % 
% % box on;
% % grid on;
% % xlabel('\sigma_1 (m)','fontsize',16); %设置x轴的标题和字体大小
% % ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
% % AX = legend([sigma23_11 sigma23_12 sigma23_21 sigma23_22], ...
% %     ['\sigma_2=',num2str(sigma2Array(1)),'m,'], ...
% %     ['\sigma_2=',num2str(sigma2Array(2)),'m,'], ...
% %     ['\sigma_2=',num2str(sigma2Array(3)),'m,'], ...
% %     ['\sigma_2=',num2str(sigma2Array(4)),'m,'], ...
% %     'Location','northeast');%设置legend位置。
% % 
% % % AX1 = legend([sigma23_11 sigma23_12], ...
% % %     ['\sigma_2=',num2str(sigma2Array(1)),'m,','\sigma_2=',num2str(sigma2Array(1)),'^{\circ}'], ...
% % %     ['\sigma_2=',num2str(sigma2Array(2)),'m,','\sigma_3=',num2str(sigma2Array(2)),'^{\circ}'], ...
% % %     'Location','northwest');%设置legend位置。
% % % LEG = findobj(AX1,'type','text');
% % % % axis([0 10 0 90]);
% % % set(LEG,'FontSize',11);%设置legend字体大小
% % % set(gca,'FontSize',16);
% % % 
% % % ah=axes('position',get(gca,'position'),'visible','off');
% % % AX2 = legend(ah,[sigma23_21 sigma23_22], ...
% % %     ['\sigma_2=',num2str(sigma2Array(2)),'m,','\sigma_3=',num2str(sigma3Array(1)),'^{\circ}'], ...
% % %     ['\sigma_2=',num2str(sigma2Array(2)),'m,','\sigma_3=',num2str(sigma3Array(2)),'^{\circ}'], ...
% % %     'Location','northeast');%设置legend位置。
% % LEG = findobj(AX,'type','text');
% % % axis([0 10 0 90]);
% % set(LEG,'FontSize',11);%设置legend字体大小
% % set(gca,'FontSize',16);
