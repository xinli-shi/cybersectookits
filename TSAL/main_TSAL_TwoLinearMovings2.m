clear;clc;
WalkDist1 = 200; % walk 10 meters for the first moving
WalkDist2 = WalkDist1; % walking distance for the second moving

TMDistArrayREAL=[WalkDist1,WalkDist2];
ThetaArrayREAL = [0 135]; % unit: degree
AnchorTCS = [100, 300]; % anchor coordinate in real coordinate system
TargetTCS = [0,0]; % target coordinate in real coordinate system

sigma1Array = 2:2:4;%1:2:9; % gaussian ranging error deviation for the anchor's measurement% unit:meter
sigma2Array = 1:2:9;%2:2:4; % gaussian error deviation for walking distance %unit:meter
sigma3Array = 1:1:2; % gaussion error deviation for target turning angle; unit: degree

N=10000
simN=1000
sigma1_N=size(sigma1Array,2);
sigma2_N=size(sigma2Array,2);
sigma3_N=size(sigma3Array,2);
ErrIteration=zeros(1,simN);
Average = zeros(sigma2_N,sigma1_N*sigma3_N);
Variance = zeros(sigma2_N,sigma1_N*sigma3_N);
i=0;

tic
for sigma2 = sigma2Array
    i=i+1
    j=0;
    for sigma1 = sigma1Array
        j=j+1
        h=0;
        for sigma3 = sigma3Array
            h=h+1
            for k=1:simN
                %[ErrIteration(k)] = MainSALRAW(Anchor,Target,TargetWalkDist,sigmaMeasureDist,sigmaWalkDist,N);
                [EstimatedTarget, ErrIteration(k)] = Proposed_TMSAL_TwoLinearMovings(AnchorTCS,TargetTCS, ...
                    ThetaArrayREAL,TMDistArrayREAL,sigma1,sigma2,sigma3,sigma3,N);
            end
            
            if j==1
                Average(i,h) = mean(ErrIteration);
                Variance(i,h) = var(ErrIteration);
            elseif j==2
                Average(i,j+h) = mean(ErrIteration);
                Variance(i,j+h) = var(ErrIteration);
            end
        end
    end
%     errorbar(sigma1Array,Average(i,:),Variance(i,:));
end
toc
save(['TMSAL_sigma1=',num2str(sigma1Array),'_sigma2=',num2str(sigma2Array),'_sigma3=',num2str(sigma3Array),'.mat'], ...
   'sigma1Array','sigma2Array','sigma3Array', 'Average','Variance');

l_sigma13_11 = 'k-s';%表示\sigma2和\sigma3都取数组的第一个值时所用的颜色和线型
l_sigma13_12 = 'k--s';
l_sigma13_21 = 'b->';
l_sigma13_22 = 'b-->';
linewidth_value=1.5;

errorbar(sigma2Array,Average(:,1),Variance(:,1),l_sigma13_11,'LineWidth',linewidth_value);hold on;
sigma23_11 = plot(sigma2Array,Average(:,1),l_sigma13_11,'LineWidth',linewidth_value);

errorbar(sigma2Array,Average(:,2),Variance(:,2),l_sigma13_12,'LineWidth',linewidth_value);
sigma23_12 = plot(sigma2Array,Average(:,2),l_sigma13_12,'LineWidth',linewidth_value);

errorbar(sigma2Array,Average(:,3),Variance(:,3),l_sigma13_21,'LineWidth',linewidth_value);
sigma23_21 = plot(sigma2Array,Average(:,3),l_sigma13_21,'LineWidth',linewidth_value);

errorbar(sigma2Array,Average(:,4),Variance(:,4),l_sigma13_22,'LineWidth',linewidth_value);
sigma23_22 = plot(sigma2Array,Average(:,4),l_sigma13_22,'LineWidth',linewidth_value);

box on;
grid on;
xlabel('\sigma_2 (m)','fontsize',16); %设置x轴的标题和字体大小
ylabel('Localization Error (m)','fontsize',16); %设置y轴的标题和字体大小
% AX = legend([sigma23_11 sigma23_12 sigma23_21 sigma23_22], ...
%     ['\sigma_2=',num2str(sigma2Array(1)),'m,','\sigma_3=',num2str(sigma3Array(1)),'m'], ...
%     ['\sigma_2=',num2str(sigma2Array(1)),'m,','\sigma_3=',num2str(sigma3Array(2)),'m'], ...
%     ['\sigma_2=',num2str(sigma2Array(2)),'m,','\sigma_3=',num2str(sigma3Array(1)),'m'], ...
%     ['\sigma_2=',num2str(sigma2Array(2)),'m,','\sigma_3=',num2str(sigma3Array(2)),'m'], ...
%     'Location','northeast');%设置legend位置。

AX1 = legend([sigma23_11 sigma23_12], ...
    ['\sigma_1=',num2str(sigma1Array(1)),'m,','\sigma_3=',num2str(sigma3Array(1)),'^{\circ}'], ...
    ['\sigma_1=',num2str(sigma1Array(1)),'m,','\sigma_3=',num2str(sigma3Array(2)),'^{\circ}'], ...
    'Location','northwest');%设置legend位置。
LEG = findobj(AX1,'type','text');
% axis([0 10 0 90]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);

ah=axes('position',get(gca,'position'),'visible','off');
AX2 = legend(ah,[sigma23_21 sigma23_22], ...
    ['\sigma_1=',num2str(sigma1Array(2)),'m,','\sigma_3=',num2str(sigma3Array(1)),'^{\circ}'], ...
    ['\sigma_1=',num2str(sigma1Array(2)),'m,','\sigma_3=',num2str(sigma3Array(2)),'^{\circ}'], ...
    'Location','northeast');%设置legend位置。
LEG = findobj(AX2,'type','text');
% axis([0 10 0 90]);
set(LEG,'FontSize',11);%设置legend字体大小
set(gca,'FontSize',16);
